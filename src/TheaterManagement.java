import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class TheaterManagement {
	private double[][] first = new double[15][20];
	private double[][] second = new double[15][20];
	private double[][] third = new double[15][20];
	private String[] workday = {"Mon","Tue","Wed","Thu","Fri"};
	private String[] holiday = {"Sun","Sat"};
	private ArrayList<String> workDay = new ArrayList(Arrays.asList(workday));
	private ArrayList<String> holiDay = new ArrayList(Arrays.asList(holiday));
	private Date date = new Date();
	private String day = date.toString();
	private String sub = day.substring(0,3);

	public double buyTicket(int time, char row, int column){
		int Row = row-65;
		double tk = DataSeatPrice.ticketPrices[Row][column];
		double rcF = first[Row][column]; //rowcolumn1
		double rcS = second[Row][column];//rowcolumn2
		double rcT = third[Row][column];//rowcolumn3
		boolean work = workDay.contains(sub);
		boolean hol = holiDay.contains(sub);
		if(work && time==1){
			double price = tk ;
			if(price!=rcF){
				rcF = Double.parseDouble((""+price));
				return price;
			}
		}
		else if(work && time==2){
			double price = tk;
			if(price!=rcS){
				rcS = Long.parseLong((""+price));
				return price;
			}
		}
		else if(hol && time==1){
			double price = tk;
			if(price!=rcF){
				rcF = Long.parseLong((""+price));
				return price;
			}
		}
		else if(hol && time==2){
			double price = tk;
			if(price!=rcS){
				rcS = Long.parseLong((""+price));
				return price;
			}
		}
		else if(hol && time==3){
			double price = tk;
			if(price!=rcT){
				rcT = Long.parseLong((""+price));
				return price;
			}
		}
		return 0;
	}
	//Buy : All time ,  Row , Column
	public double buyTicket(char row, int column){
		int Row = row-65;
		double tk = DataSeatPrice.ticketPrices[Row][column];
		double rcF = first[Row][column];
		double rcS = second[Row][column];
		double rcT = third[Row][column];
		boolean work = workDay.contains(sub);
		boolean hol = holiDay.contains(sub);
		if(work){
			double price = tk;
			if(price!=rcF){
				rcF = Double.parseDouble((""+price));
				return price;
			}
			else if(price!=rcS){
				rcS = Double.parseDouble((""+price));
				return price;
			}	
		}
			
		else if(hol){
			double price = tk;
			if(price!=rcF){
				rcF = Double.parseDouble((""+price));
				return price;
			}
			if(price!=rcS){
				rcS = Double.parseDouble((""+price));
				return price;
			}
			if(price!=rcT){
				rcT = Double.parseDouble((""+price));
				return price;
			}
		}
		return 0;
	}
	public void buyTicket(long price,int time,String day){
		Price p = new Price();
		p.buyTicket(price, time, day);
	}
}
